import logging

class Logger:
    def __init__(self, filepath=None, verbose=False):
        self.logger = logging.getLogger(__name__)
        # set verbosity level
        if verbose:
            self.logger.setLevel(logging.DEBUG)
        else:
            self.logger.setLevel(logging.INFO)
        # add file handler
        if filepath:
            self.handler = logging.FileHandler(filepath)
            self.formatter = logging.Formatter('%(asctime)s:  %(message)s')
            self.handler.setFormatter(self.formatter)
            self.logger.addHandler(self.handler)
        # add console handler
        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(logging.Formatter('%(message)s'))
        self.logger.addHandler(consoleHandler)

    def write_info(self, message):
        self.logger.info(message)

    def write_debug(self, message):
        self.logger.debug(message)
