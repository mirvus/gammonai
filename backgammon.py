import argparse
import os
import time
import sys

import game as g
import utils as utils
from logger import Logger

from agents.random_agent import RandomAgent
from agents.simple_agent import SimpleAgent
from agents.expectimax_pipcount import ExpectimaxPipcountAgent
from agents.expectimax_door import ExpectimaxDoorAgent
from agents.expectimax_mixed import ExpectimaxMixedAgent
from agents.reflex_mixed import ReflexMixedAgent

def play_multiple(players, gameobj, gamenum=100, logger=None):
    """Play multiple games of backgammon."""
    scores = [0, 0]
    for i in range(gamenum):
        gameobj = g.Game(g.LAYOUT)
        white_won = play_game(players, gameobj, draw=False, logger=logger)
        scores[int(white_won)] += 1
    if logger:
        logger.write_info("score({} games): {}={}, {}={}\n"
        .format(gamenum, players[0], scores[0], players[1], scores[1]))

def play_game(players, gameobj, draw=False, logger=None):
    """Play single game of backgammon."""
    gameobj.new_game()
    # choose starting player and mark him as current player
    cur_player = int(utils.choose_first_player(logger))
    # main game loop
    over = False
    while not over:
        roll = utils.roll_dice()
        if draw:
            gameobj.draw(roll)
        # switch current player
        cur_player =  (cur_player+1)%2
        if cur_player:
            gameobj.reverse()
        play_turn(players[cur_player], gameobj, roll, logger)
        if cur_player:
            gameobj.reverse()
        over = gameobj.is_over()
    winner = gameobj.winner()
    if logger:
        logger.write_info(str(players[int(winner)]) + " won")
    return winner

def play_turn(player, gameobj, roll, logger=None):
    """Play single turn for a player."""
    moves = gameobj.getActions(roll, gameobj.players[0], nodups=True)
    move = player.pickMove(moves, gameobj)
    if logger:
        logger.write_debug("{}: {}".format(player, utils.format_move(move)))
    if move:
        gameobj.takeAction(move, gameobj.players[0])

def pick_agent(agent_name, playernum):
    """Pick agent based on agent_name.
    Playernum 0 is for first player, 1 for second player.
    If no name is supplied RandomAgent is selected."""
    player = g.Game.TOKENS[playernum]
    if agent_name == 'random':
        return RandomAgent(player)
    elif agent_name == 'human':
        raise NotImplementedError("Human player not implemented yet.")
    elif agent_name == 'hit':
        return SimpleAgent(player)
    elif agent_name == 'door':
        return ExpectimaxDoorAgent(player)
    elif agent_name == 'mixed':
        return ExpectimaxMixedAgent(player)
    elif agent_name == 'reflexmixed':
        return ReflexMixedAgent(player)
    elif agent_name == 'pipcount':
        return ExpectimaxPipcountAgent(player)
    else:
        raise ValueError("invalid agent name: '{}'".format(agent_name))

def parse_arguments():
    """parse input arguments"""
    parser = argparse.ArgumentParser()
    parser.add_argument('--white', '-w', default="random",
        help="white agent (random, hit, door, pipcount, reflexmixed, mixed)")
    parser.add_argument('--black', '-b', default="random",
        help="black agent (random, hit, door, pipcount, reflexmixed, mixed)")
    parser.add_argument('--no-gui', '-g', action='store_true', default=False,
                        help="don't show GUI")
    parser.add_argument('-n', type=int, default=1, help="number of games")
    parser.add_argument('--verbose', '-v', action='store_true', default=False,
                        help="print verbose output")
    parser.add_argument('--file', '-f', default='logs/gammon.log', help="save logs to file")
    return parser.parse_args()

def init_logger(path):
    folder = os.path.dirname(path)
    if not os.path.exists(folder):
        os.makedirs(folder)

def main(args=None):
    # parse arguments
    args = parse_arguments()
    init_logger(args.file)
    logger = Logger(args.file, verbose=args.verbose)
    logger.write_info("NEW GAME")
    logger.write_info("o={}, x={}, n={}, verbose={}, no_gui={}"
            .format(args.white, args.black, args.n, args.verbose, args.no_gui))
    # draw unless no-gui option is supplied
    draw = not args.no_gui
    # play game
    gameobj = g.Game(g.LAYOUT)
    # pick player agents
    players = (pick_agent(args.white, 0),
               pick_agent(args.black, 1))
    if args.n > 1:
        play_multiple(players, gameobj, gamenum=args.n, logger=logger)
    else:
        play_game(players, gameobj, draw, logger)
        if draw:
            gameobj.draw()
            time.sleep(1)
            gameobj.quit_game()
    sys.exit()

if __name__ == '__main__':
    main()
