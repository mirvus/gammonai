import random

def roll_dice():
    """roll two 6-sided dices"""
    return (random.randint(1, 6), random.randint(1, 6))

def format_move(move):
    if move == None:
        return "pass"
    move_str = ""
    for m_from, m_to in move:
        move_str += str(m_from) + "/" + str(m_to) + " "
    return move_str

def choose_first_player(logger=None):
    """Decides which player starts the the game.
    Returns:
        True - first(white) player starts,
        False - black player starts.
    """
    while True:
        white = sum(roll_dice())
        black = sum(roll_dice())
        if logger:
            logger.write_debug("white rolls {}, black rolls {}".format(white, black))
        diff = white - black
        if diff and logger:
            logger.write_debug("white starts" if white > black else "black starts")
        if white > black:
            return True
        elif white < black:
            return False
