class BasicAgent:
    """BasicAgent class. Used as base class for other agents."""
    def __init__(self, player):
        self.player = player
    
    def pickMove(self, moves, gameobj=None):
        raise NotImplementedError("Not Implemented")

    def __str__(self):
        return self.player
