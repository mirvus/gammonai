import random
import time

from agents.basic import BasicAgent
from agents.evalfuncs import simple_evalfunc

class SimpleAgent(BasicAgent):
    """SimpleAgent uses finds best move with no lookahead."""

    def pickMove(self, moves, gameobj=None):
        if not moves:
            return None
        move_score = []
        for move in list(moves):
            g = gameobj.clone()
            g.takeAction(move, self.player)
            move_score.append((move, simple_evalfunc(g, self.player)) )
        return max(move_score)[0]
