import random

from agents.basic import BasicAgent

class RandomAgent(BasicAgent):
    """
    RandomAgent picks random move from list of available moves.
    """

    def pickMove(self, moves, gameobj=None):
        return random.choice(list(moves)) if moves else None
