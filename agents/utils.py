def single_pieces(gameobj, player):
    count = 0
    for col in gameobj.grid:
        if len(col) == 1 and col[0] == player:
            count += 1
    return count

def count_enemy_bar_pieces(gameobj, player):
    """returns total count of enemy pieces on bar"""
    enemy = 'o' if player == 'x' else 'x'
    return len(gameobj.barPieces[enemy])

def count_blocked_points(gameobj, player):
    """returns total count of blocked points
    (player has at least two pieces)."""
    blocked = 0
    for col in gameobj.grid:
        if col.count(player) >= 2:
            blocked += 1
    return blocked

def count_home_points(gameobj, player):
    """returns number of created points in enemy home"""
    homepoints = 0
    homerow = range(0,6) if player == 'x' else range(18,24)
    for i in homerow:
        col = gameobj.grid[i]
        if col.count(player) >= 2:
            homepoints += 1
    return homepoints

def pipcount(gameobj, player):
    """returns total pipcount(total moves needed to win)."""
    count = 0
    for i, col in enumerate(gameobj.grid):
        pieces = col.count(player)
        if pieces:
            if player == 'o':
                count += (24-i) * pieces
            elif player == 'x':
                count += i * pieces
    count += len(gameobj.barPieces[player]) * 24
    return count
