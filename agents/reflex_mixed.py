import random
import time
import itertools as it


from agents.basic import BasicAgent
from agents.evalfuncs import mixed_evalfunc


class ReflexMixedAgent(BasicAgent):
    """
    ReflexMixedAgent uses mixed strategy with no lookahead.
    """

    def pickMove(self, moves, gameobj=None):
        if not moves:
            return None
        move_score = []
        for move in list(moves):
            g = gameobj.clone()
            g.takeAction(move, self.player)
            move_score.append((move, mixed_evalfunc(g, self.player)) )
        return max(move_score)[0]
