from agents.utils import *

def simple_evalfunc(gameobj, player):
    """simple heuristic evaluation function."""
    score = 0
    score += 1 * len(gameobj.barPieces[gameobj.opponent(player)])
    score -= 1 * len(gameobj.barPieces[player])
    return score

def hit_evalfunc(gameobj, player):
    """
    Evaluation function that maximizes difference in hit pieces.
    score = 1 * enemy_bar - 1 * own_bar
    """
    score = 0
    score += 1 * count_enemy_bar_pieces(gameobj, player)
    score -= 1 * len(gameobj.barPieces[player])
    return score

def mixed_evalfunc(gameobj, player):
    """
    Evaluation function that uses number of heuristics.
    score = 2 * home_points + 1 * points + 1 * enemy_bar - 1 * own_bar
    """
    score = 2 * count_home_points(gameobj, player)
    score += 1 * count_blocked_points(gameobj, player)
    score += 1 * count_enemy_bar_pieces(gameobj, player)
    score -= 1 * len(gameobj.barPieces[player])
    return score

def door_evalfunc(gameobj, player):
    """Evaluation function that maximizes doors(points with at least 2 pieces)."""
    score = 0
    score += 1 * count_blocked_points(gameobj, player)
    return score

def pipcount_evalfunc(gameobj, player):
    """evaluation function that uses pipcount as score"""
    score = pipcount(gameobj, player)
    if score == 0:
        return 1000
    return 1 / float(score)

