from agents.basic import BasicAgent
from agents.evalfuncs import mixed_evalfunc


class ExpectimaxMixedAgent(BasicAgent):
    """
    ExpectimaxMixedAgent uses expectiminimax strategy and mixed strategy.
    """
    def pickMove(self, moves, gameobj=None):
        return self.expectimaxDecision(moves, gameobj, 1)


    def expectimaxDecision(self, moves, gameobj, level):
        """
        expectimaxDecision finds best move from moves.
        Uses expectimax algorithm.
        """
        # too broad tree, use only reflex eval
        if len(moves) > 100:
            level = 0
        values_moves = []
        for move in moves:
            value = self.singleMove(gameobj, move, self.player, level)
            values_moves.append((value, move))
        if len(values_moves) == 0:
            return None
        return max(values_moves)[1]


    def chanceNode(self, gameobj, player, level):
        """
        chanceNode represents DICE node in expectimax game tree
        """
        if level == 0:
            return mixed_evalfunc(gameobj, player)

        value = 0
        for dice1 in range(1, 7):
            for dice2 in range(dice1+1, 7):
                roll_chance = 2 if dice1 == dice2 else 1
                value = roll_chance * self.minimaxDecision(gameobj, player, (dice1, dice2), level)
        return value / 36.0


    def minimaxDecision(self, gameobj, player, roll, level):
        """
        minimaxDecision represents MIN or MAX node in expectimax tree.
        """
        moves = gameobj.getActions(roll, player, nodups=True)
        if player != self.player:
            level -= 1 
        if len(moves) == 0:
            return self.chanceNode(gameobj, gameobj.opponent(player), level)
        values = []
        for move in moves:
            value = self.singleMove(gameobj, move, player, level)
            values.append(value)
        if player == self.player:
            return max(values)
        else:
            return min(values)


    def singleMove(self, gameobj, move, player, level):
        """
        singleMove is a wrapper for chance node. Does not alter gameobj state.
        """
        actions = gameobj.takeAction(move, player)
        enemy_player = gameobj.opponent(player)
        value = self.chanceNode(gameobj, enemy_player, level)
        gameobj.undoAction(move, player, actions)
        return value
