# Gammon AI

Project aims at constructing and comparing different computer agents for backgammon game.

# Requirements

- Python 2.7
- Pygame

# Usage

To print help use:

`python backgammon.py -h`

Play 100 games in console mode with pipcount agent as white and random agent as black:

`python backgammon.py -w pipcount -b random --no-gui -n 100`

Play game with GUI and verbose output (pipcount vs random):

`python backgammon.py -w pipcount -v`

# Available agents

- random (default) - random agent picks moves at random
- hit - reflex agent that tries to hit enemy pieces
- door - expectimax agent that prioritizes creating doors(e.g. points with at least 2 own pieces)
- pipcount - expectimax agent that uses pipcount as evaluation function
- mixed - expectimax agent that uses mixed strategy
- reflexmixed - reflex agent with same strategy as mixed

# Logs 

By default logs are stored at logs/gammon.log in project directory.

# Results

Table below shows results of 1000 games played between each two agents.
First number is games won by agent in row, second - agent in column.

|                     | random    | reflex hit | expectimax door | expectimax pipcount | expectimax mixed | reflex mixed |
|---------------------|-----------|------------|-----------------|---------------------|------------------|--------------|
| random              |           | 484 \ 516  | 377 \ 627       | 215 \ 785           | 195 \ 805        | 499 \ 501    |
| reflex hit          | 516 \ 484 |            | 440 \ 560       | 381 \ 619           | 290 \ 710        | 493 \ 507    |
| expectimax door     | 627 \ 377 | 560 \ 440  |                 | 462 \ 538           | 424 \ 576        | 568 \ 432    |
| expectimax pipcount | 785 \ 215 | 619 \ 381  | 538 \ 462       |                     | 339 \ 661        | 569 \ 431    |
| expectimax mixed    | 805 \ 195 | 710 \ 290  | 576 \ 424       | 661 \ 339           |                  | 624 \ 376    |
| reflex mixed        | 501 \ 499 | 507 \ 493  | 432 \ 568       | 431 \ 569           | 376 \ 624        |              |
